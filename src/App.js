import logo from './logo.svg';
import './App.css';
import Playlist from './Components/Playlist';
import Filter from "./Components/Filter";
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/style.css';


function App() {
    return (
        <div className="App">
            <div className="container">
                <div className="row no-gutters mt-4">
                    <div className="col">
                        <div className="mb-4">

                        </div>
                        <div>
                            <Playlist/>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    );
}

export default App;
