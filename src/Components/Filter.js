import React, {Component} from 'react';

const API_HOST = 'http://playlist-api.zzz.com.ua/';

class Filter extends Component {
    constructor(props) {
        super(props);

        this.state = {
            error: null,
            isLoaded: false,
            years: [],
            authors: [],
            genres: [],
            year: '',
            author: '',
            genre: '',
            reset: false
        };

        this.handleYearChange = this.handleYearChange.bind(this);
        this.handleAuthorChange = this.handleAuthorChange.bind(this);
        this.handleGenreChange = this.handleGenreChange.bind(this);
        this.resetFilter = this.resetFilter.bind(this);
    }

    componentDidMount() {
        this.getFilterData();
    }

    handleYearChange(event) {
        this.setState({
            year: event.target.value,
            reset: true
        }, () => {
            this.sendData();
        });
    }

    handleAuthorChange(event) {
        this.setState({
            author: event.target.value,
            reset: true
        }, () => {
            this.sendData();
        });
    }

    handleGenreChange(event) {
        this.setState({
            genre: event.target.value,
            reset: true
        }, () => {
            this.sendData();
        });
    }

    resetFilter() {
        this.setState( {
            year: '',
            author: '',
            genre: '',
            reset: false
        }, () => {
            this.sendData();
        });
    }

    sendData = () => {
        this.props.parentCallback({
            year: this.state.year,
            genre: this.state.genre,
            author: this.state.author
        });
    };

    render() {
        const {years, authors, genres, year, author, genre, reset} = this.state;
        return (
            <div className="form">
                <select className="form-select" value={author} onChange={this.handleAuthorChange}>
                    <option selected={true}>Автор</option>

                    {authors.map(item => (
                        <option key={item.id} value={item.id}>
                            {item.name}
                        </option>
                    ))}
                </select>
                <select className="form-select ml-2" value={year} onChange={this.handleYearChange}>
                    <option selected={true}>Год</option>

                    {years.map(item => (
                        <option key={item.year} value={item.year}>
                            {item.year}
                        </option>
                    ))}
                </select>
                <select className="form-select ml-2" value={genre} onChange={this.handleGenreChange}>
                    <option selected={true}>Жанр</option>

                    {genres.map(item => (
                        <option key={item.id} value={item.id}>
                            {item.name}
                        </option>
                    ))}
                </select>
                {reset && (
                    <button className="button" onClick={this.resetFilter}>
                        <img className="image-btn" src={'./cancel.png'}/>
                    </button>
                )}
            </div>
        );
    }

    getFilterData() {
        fetch(API_HOST + '/years')
            .then(res => res.json())
            .then(
                (result) => {

                    this.setState({
                        isLoaded: true,
                        years: result.data
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
        fetch(API_HOST + '/authors')
            .then(res => res.json())
            .then(
                (result) => {

                    this.setState({
                        isLoaded: true,
                        authors: result.data
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
        fetch(API_HOST + '/genres')
            .then(res => res.json())
            .then(
                (result) => {

                    this.setState({
                        isLoaded: true,
                        genres: result.data
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }
}

export default Filter;