import React, {Component} from 'react';
import Filter from "./Filter";

const API_HOST = 'http://playlist-api.zzz.com.ua/';

export default class Playlist extends Component {
    constructor(props) {
        super(props);

        let filter = {
            year: '',
            author: '',
            genre: ''
        };
        this.state = {
            error: null,
            isLoaded: false,
            items: [],
            years: [],
            authors: [],
            genres: [],
            filter: filter,
            sortYear: false,
            sortParams: []
        };
        this.sort = this.sort.bind(this);
    }
    callbackFunction = (filterData) => {
        if (filterData)
            this.setState({
                filter: filterData
            }, () => {
                this.getData();
            });
    };

    componentDidMount() {
        this.callbackFunction();
        this.getData();
    }

    sort(param) {

        let list = this.state.sortParams;

        const index = list.indexOf(param);

        if (index > -1) {
            list.splice(index, 1);
        } else
            list.push(param);

        this.setState({
            sortParams: list
        }, () => {
            this.getData();
        });
    }

    getData() {
        let filters = this.state.filter;

        let request = 'year=' + filters.year + '&genre=' + filters.genre + '&author=' + filters.author;

        this.state.sortParams.map((item) => {
            console.log(item);
            request += '&sort[]=' + item;
        });


        fetch(API_HOST + "/tracks?" + request)
            .then(res => res.json())
            .then(
                (result) => {

                    this.setState({
                        isLoaded: true,
                        items: result.data
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }
    render() {
        const { error, isLoaded, items, years } = this.state;

        if (error) {
            return <div>Ошибка: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Загрузка...</div>;
        } else {
            return (
                <div>
                    <div>
                        <Filter parentCallback = {this.callbackFunction.bind(this)}/>
                    </div>
                    <div className="row mt-4">
                        <div className="col-3 border bg-light">Название <button className="button" onClick={this.sort.bind(this, 'name')}>
                            <img className="image-btn" src={'./sort.png'}/>
                        </button>
                        </div>
                        <div className="col-3 border bg-light" >Исполнитель
                            <button className="button" onClick={this.sort.bind(this, 'author')}>
                                <img className="image-btn" src={'./sort.png'}/></button>
                        </div>
                        <div className="col-3 border bg-light">Жанр
                            <button className="button" onClick={this.sort.bind(this, 'genre')}>
                                <img className="image-btn" src={'./sort.png'}/></button>
                        </div>
                        <div className="col-3 border bg-light">Год
                            <button className="button" onClick={this.sort.bind(this, 'year')}>
                                <img className="image-btn" src={'./sort.png'}/></button>
                        </div>
                    </div>
                    {items.map(item => (
                        <div className="row">
                            <div className="col-3 border">{item.name}</div>
                            <div className="col-3 border">{item.author.name}</div>
                            <div className="col-3 border">{item.genre.name}</div>
                            <div className="col-3 border">{item.year}</div>
                        </div>
                    ))}
                </div>
            );
        }
    }

}
// export default Playlist;